﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BibliotecaAluguel.Telas
{
    public partial class frmCadastrar : Form
    {
        public frmCadastrar()
        {
            InitializeComponent();
        }

        private void BunifuFlatButton1_Click(object sender, EventArgs e)
        {
            try
            {
                // chama o database
                DataBase.model.tb_login l = new DataBase.model.tb_login();

                if (txtuser.Text == string.Empty || txtsenha.Text == string.Empty || txtemail.Text == string.Empty)
                {
                    MessageBox.Show("complete os campos");
                    return;
                }

                // carrega o modelo
                l.nm_usuario = txtuser.Text;
                l.nm_senha = txtsenha.Text;
                l.nm_email = txtemail.Text;

                // passa para o banco
                DataBase.model.libraryEntities1 db = new DataBase.model.libraryEntities1();
                db.tb_login.Add(l);

                // salva no banco
                db.SaveChanges();

                MessageBox.Show("obrigado","sucesso",MessageBoxButtons.OK,MessageBoxIcon.Information);

                Telas.frmMenuLogin tela = new frmMenuLogin();
                tela.Show();
                this.Hide();

            }
            catch (Exception)
            {

                MessageBox.Show("informações invalidas","erro");
            }
        }
    }
}

            

