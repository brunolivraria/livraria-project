﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BibliotecaAluguel
{
    public partial class frmInsert : Form
    {
        public frmInsert()
        {
            InitializeComponent();
            // carrega a função combo assim que aberto o programa 
            this.CarregarCombo();
        }

        private void CarregarCombo()
        {
            // faz a consulta no banco de dados 
            DataBase.databaselibrary db = new DataBase.databaselibrary();
            List<DataBase.model.tb_categorias> categorias = db.Listar2();
            // carrega as informações no combo 
            cboCategorias.DisplayMember = nameof(DataBase.model.tb_categorias.nm_categoria);
            cboCategorias.DataSource = categorias;
        }

        private void Btn_Click(object sender, EventArgs e)
        {
            if (cboCategorias.Text == string.Empty || txtLivro.Text == string.Empty 
                || txtAutor.Text == string.Empty || dtpData.Value == null)
            {
                MessageBox.Show("complete os campos", "erro");
                return;
            }

            DateTime data = DateTime.Now;
            if (dtpData.Value > data )
            {
                MessageBox.Show("informe uma data valida","erro");
                return;
            }
            // chama o modelo 
            DataBase.model.tb_categorias Turmas = cboCategorias.SelectedItem as DataBase.model.tb_categorias;
            int id = Turmas.id_categoria;
            // passa os parâmetros
            DataBase.model.tb_livros l = new  DataBase.model.tb_livros();
            l.id_categoria = id;
            l.nm_livro = txtLivro.Text;
            l.nm_autor = txtAutor.Text;
            l.nr_avaliaçao = Convert.ToInt32(nudAvaliação.Value);
            l.dt_publicaçao = dtpData.Value.Date;
            // chama a o database
            DataBase.databaselibrary db = new DataBase.databaselibrary();
            db.insert(l);

            MessageBox.Show("inserido");

        }
    }
}
